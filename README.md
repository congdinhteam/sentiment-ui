
# Sentiment (Using Django-React-Redux-Bootstrap 4)

## Running
### Setup
- On project root, do the following:
- Create a copy of ``{{project_name}}/settings/local.py.example``:  
  `cp {{project_name}}/settings/local.py.example {{project_name}}/settings/local.py`
- Create a copy of ``.env.example``:  
  `cp .env.example .env`
- Create the migrations for `users` app (do this, then remove this line from the README):  
  `python manage.py makemigrations`
- Run the migrations:  
  `python manage.py migrate`

### Tools
- Setup [editorconfig](http://editorconfig.org/), [prospector](https://prospector.landscape.io/en/master/) and [ESLint](http://eslint.org/) in the text editor you will use to develop.

### Running the project
- `pip install -r requirements.txt`
- `npm install`
- `npm run start`
- `python manage.py runserver`

### Testing
`make test`

Will run django tests using `--keepdb` and `--parallel`. You may pass a path to the desired test module in the make command. E.g.:

`make test someapp.tests.test_views`

